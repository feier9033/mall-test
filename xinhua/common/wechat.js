// 自己封装的 uni.request 工具类
let jweixin = require("jweixin-module");

export default {
	//判断是否在微信中  
	isWechat: function() {
		var ua = window.navigator.userAgent.toLowerCase();
		if (ua.match(/micromessenger/i) == 'micromessenger') {
			//  console.log('是微信客户端')
			return true;
		} else {
			//  console.log('不是微信客户端')
			return false;
		}
	},
	//初始化sdk配置  
	initJssdkShare: function(callback, url) {
		console.log("init Url : " + url)
		// 这是我这边封装的 request 请求工具，实际就是 uni.request 方法。
		uni.request({
			url: 'https://hd.xjsfjgxx.com/prod-api/wx/redirect/wxf4534cbe771c8a65/getJsapiTicket',
			data:{url:url},
			success: function(res) {
				let success = res.data.success;
				let result = res.data.data;
				if (success) {
					//console.log(result)
					jweixin.config({
						debug: false,
						appId: result.appId,
						timestamp: result.timestamp,
						nonceStr: result.nonceStr,
						signature: result.signature,
						jsApiList: [
							"onMenuShareTimeline", //分享给好友
							"onMenuShareAppMessage", //分享到朋友圈
							"checkJsApi"
						]
					});
					//配置完成后，再执行分享等功能  
					if (callback) {
						callback(result);
					}
				}
			}
		})
	},
	//在需要自定义分享的页面中调用  
	share: function(data, url) {
		url = url ? url : window.location.href;
		console.log("url:" + url)
		if (!this.isWechat()) {
			return;
		}
		//每次都需要重新初始化配置，才可以进行分享  
		this.initJssdkShare(function(signData) {
			jweixin.ready(function() {
				var shareData = {
					title: data && data.title ? data.title : signData.site_name,
					desc: data && data.desc ? data.desc : signData.site_description,
					link: url,
					imgUrl: data && data.img ? data.img : signData.site_logo,
					success: function(res) {
						// 分享后的一些操作,比如分享统计等等
					},
					cancel: function(res) {}
				};
				//分享给朋友接口  
				jweixin.onMenuShareAppMessage(shareData);
				//分享到朋友圈接口  
				jweixin.onMenuShareTimeline(shareData);
			});
		}, url);
	},
}
